import React, { useState } from 'react'
import { Dimensions, View, ScrollView, TouchableWithoutFeedback, KeyboardAvoidingView, Keyboard, Platform, NativeModules, LayoutAnimation } from 'react-native'

const { UIManager } = NativeModules
UIManager.setLayoutAnimationEnabledExperimental &&
    UIManager.setLayoutAnimationEnabledExperimental(true)

type Mode = 'default' | 'debug'

type Props = {
    mode?: Mode
    backgroundColor?: string
    width?: string | number
    keyboardVerticalOffset?: number
    disableScrollView?: boolean
    children: React.ReactNode
}

const screenHeight = Dimensions.get('screen').height

const KeyboardBehavior: React.FC<Props> = ({
    mode = "default",
    backgroundColor = "transparent",
    width = "100%",
    keyboardVerticalOffset = 0,
    children,
    disableScrollView = false
}) => {

    const [state, setState] = useState({ keyboardHeight: 0 })

    React.useEffect(() => {
        // <=> ComponentDidMount
        const keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', keyboardDidShow)
        const keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', keyboardDidHide)
        // ci-dessous pour iOS parce que WillShow et WillHide c'est mieux
        const keyboardWillShowListener = Keyboard.addListener('keyboardWillShow', keyboardWillShow)
        const keyboardWillHideListener = Keyboard.addListener('keyboardWillHide', keyboardWillHide)
        // <=> ComponentWillUnmount
        return () => {
            if (mode === 'debug') {
                keyboardDidShowListener.remove()
                keyboardDidHideListener.remove()
                keyboardWillShowListener.remove()
                keyboardWillHideListener.remove()
            }
        }
    }, [])

    const keyboardDidShow = (e) => {
        if (mode === 'debug') setState({ ...state, keyboardHeight: screenHeight - e.endCoordinates.screenY })
    }

    const keyboardWillShow = (e) => {
        if (mode === 'debug') setState({ ...state, keyboardHeight: screenHeight - e.endCoordinates.screenY })
    }

    const keyboardDidHide = (e) => {
        if (mode === 'debug') {
            LayoutAnimation.easeInEaseOut()
            setState({ ...state, keyboardHeight: 0 })
        }
    }

    const keyboardWillHide = (e) => {
        if (mode === 'debug') {
            LayoutAnimation.easeInEaseOut()
            setState({ ...state, keyboardHeight: 0 })
        }
    }

    if (mode === 'default') {
        const TagScrollView = disableScrollView ? React.Fragment : ScrollView


        return (
            <KeyboardAvoidingView keyboardVerticalOffset={keyboardVerticalOffset}
                style={{ flex: 1, backgroundColor: backgroundColor, width: width }} behavior={Platform.OS === "ios" ? "padding" : "height"}>
                <TagScrollView>{children}</TagScrollView>
            </KeyboardAvoidingView>
        )
    }

    else if (mode === 'debug') {

        return (
            <ScrollView
                style={{ backgroundColor: backgroundColor, width: width, flex: 1 }}
            >
                <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
                    <>
                        {children}
                        <View style={{ height: Platform.OS === 'ios' ? state.keyboardHeight : 0 }} />
                    </>
                </TouchableWithoutFeedback>
            </ScrollView>
        )
    }
}

export default KeyboardBehavior
