**A react-native module to help you with the keyboard behavior. It is mostly a little upgrade of KeyboardAvoidingView (default mode) with an alternative in some cases of bug (debug mode).**

<br>

**HOW TO INSTALL**
```
npm i react-native-keyboard-behavior
```
```
yarn add react-native-keyboard-behavior
```

*Condition: on Android, you need android:windowSoftInputMode set to "adjustResize" in AndroidManifest.*
<br><br>

**THE MODES**
<br>

**- "Default":** the keyboard will appear on top of the content when showing up and some space is added automatically. <br>
The screen will scroll to the focused input if covered up.

<br>

**- "Debug":** the keyboard will appear on top of the content when showing up and some space of the height of the keyboard is added manually. <br>
The screen won't scroll to the focused input if covered up.

<br>

**GUIDELINES**

1- Use this component as a container, as the highest View component of your component.<br>
*NB : You can use it around App only if you won't use the prop disableScrollView (see point 2) or encounter any flickering with the keyboard (see point 4).*

<br>

2- By default, a ScrollView is added around the children of KeyboardBehavior. So if children is already nested with a ScrollView, you might need to disable the ScrollView of the KeyboardBehavior with the prop disableScrollView.

<br>

3- If you are in default mode, you'll need to specify the prop keyboardVerticalOffset. This is the distance between the top of the user screen and the react native view. For example, with react-navigation you should add the header height. If you are not in a safe area, you will also need to add the top inset.

<br>

4- On Android, there can be some issues (mostly flickering) when the keyboard appears or disappears, especially if you use react-navigation. If you encounter thoses kind of bugs and if your(s) input(s) are never covered up by the keyboard (e.g are on top of the screen), then you can use the value "debug" to the prop behavior. You won't have any visual bug. 


<br><br>
*To better understand how the default mode works, you can read about <a href="https://reactnative.dev/docs/keyboardavoidingview" target="_blank">KeyboardAvoidingView</a>.*
<br><br>
Standard usage:
```
<KeyboardBehavior keyboardVerticalOffset={headerHeight+insets.top} backgroundColor="black">
  <View>
    <TextInput
    placeholder="test"
    />
  </View>
</KeyboardBehavior>
```
<br><br>

| **Props (all optionals)**             | **Type**             | **Default**   |
| ------------------------------------- | -------------------- | ------------- |
| behavior                              | 'default' or 'debug' | 'default'     |
| disableScrollView (default mode only) | boolean              | false         |
| backgroundColor                       | string               | 'transparent' |
| keyboardVerticalOffset                | number               | 0             |
| width                                 | string or number     | "100%"        |

